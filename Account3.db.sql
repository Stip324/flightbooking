BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "FTB" (
	"flightNumber"	INTEGER NOT NULL UNIQUE,
	"departure"	TEXT NOT NULL,
	"arrival"	TEXT NOT NULL,
	"departday"	TEXT NOT NULL,
	"arriveday"	TEXT NOT NULL,
	"price"	INTEGER NOT NULL,
	FOREIGN KEY("arrival") REFERENCES "Airport"("Name"),
	PRIMARY KEY("flightNumber"),
	FOREIGN KEY("departure") REFERENCES "Airport"("Name")
);
CREATE TABLE IF NOT EXISTS "Airport" (
	"Code"	TEXT NOT NULL UNIQUE,
	"Name"	TEXT NOT NULL UNIQUE,
	"flightCapacity"	INTEGER NOT NULL,
	PRIMARY KEY("Code")
);
CREATE TABLE IF NOT EXISTS "Booking" (
	"BookingID"	INTEGER NOT NULL UNIQUE,
	"CustomerID"	INTEGER NOT NULL,
	"FlightNum"	INTEGER NOT NULL,
	FOREIGN KEY("CustomerID") REFERENCES "User"("UserID"),
	PRIMARY KEY("BookingID"),
	FOREIGN KEY("FlightNum") REFERENCES "FTB"("flightNumber")
);
CREATE TABLE IF NOT EXISTS "Aircraft" (
	"AircraftID"	INTEGER NOT NULL,
	"Model"	TEXT NOT NULL,
	"TotalSeats"	INTEGER NOT NULL,
	"FlightNum"	INTEGER,
	PRIMARY KEY("AircraftID"),
	FOREIGN KEY("FlightNum") REFERENCES "FTB"("flightNumber")
);
CREATE TABLE IF NOT EXISTS "User" (
	"UserID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"Forename"	TEXT NOT NULL,
	"Surname"	TEXT NOT NULL,
	"Email"	TEXT NOT NULL UNIQUE,
	"Username"	TEXT NOT NULL UNIQUE,
	"Password"	TEXT NOT NULL,
	"Admin"	INTEGER
);
INSERT INTO "FTB" VALUES (101,'China','Dubai','2020-05-10 13:30','2020-05-10 20:45',500);
INSERT INTO "FTB" VALUES (201,'China','United States','2020-04-12 05:44','2020-04-12 19:20',300);
INSERT INTO "FTB" VALUES (211,'United States','Hong Kong','2020-05-12 12:30','2020-05-12 23:00',300);
INSERT INTO "FTB" VALUES (230,'United States','China','2020-04-11 15:15','2020-04-13 03:15',200);
INSERT INTO "FTB" VALUES (241,'Hong Kong','Dubai','2020-05-22 13:20','2020-05-22 18:22',300);
INSERT INTO "FTB" VALUES (340,'Hong Kong','Dubai','2020-05-22 11:35','2020-05-22 21:50',150);
INSERT INTO "FTB" VALUES (380,'Dubai','Hong Kong','2020-05-20 13:20','2020-05-20 20:20',150);
INSERT INTO "FTB" VALUES (381,'Dubai','Hong Kong','2020-05-20 12:00','2020-05-20 22:00',200);
INSERT INTO "FTB" VALUES (382,'Dubai','Hong Kong','2020-05-20 17:45','2020-05-21 02:55',150);
INSERT INTO "Airport" VALUES ('ATL','United States',1000);
INSERT INTO "Airport" VALUES ('PEK
','China',2000);
INSERT INTO "Airport" VALUES ('DXB','Dubai',3000);
INSERT INTO "Airport" VALUES ('HKG','Hong Kong',500);
INSERT INTO "Booking" VALUES (10,33,200);
INSERT INTO "Booking" VALUES (11,34,200);
INSERT INTO "Booking" VALUES (12,34,100);
INSERT INTO "Booking" VALUES (21,35,112);
INSERT INTO "Booking" VALUES (22,35,112);
INSERT INTO "Booking" VALUES (23,35,112);
INSERT INTO "Booking" VALUES (25,37,380);
INSERT INTO "Aircraft" VALUES (1,'A380',1,NULL);
INSERT INTO "User" VALUES (2,'Steven','Ip','stip324@gmail.com','stip324','stip324',1);
INSERT INTO "User" VALUES (5,'Tiana','Yip','tiana@gmail.com','Tiana','tiana123',0);
INSERT INTO "User" VALUES (11,'abc','abc','abc@gmail.com','abc','abc',0);
INSERT INTO "User" VALUES (12,'asd','asd','asd@gmail.com','asd','asd',0);
INSERT INTO "User" VALUES (13,'zxc','zxc','zxc@gmail.com','zxc','zxc',0);
INSERT INTO "User" VALUES (15,'Noor','Mohamed','nu.nu2468.hotmail.com','ghjf','password',0);
INSERT INTO "User" VALUES (16,'toby','wong','tw@uea.ac.uk','tw','tw',0);
INSERT INTO "User" VALUES (19,'admin','admin','admin@uea.com','admin','admin',1);
INSERT INTO "User" VALUES (20,'admin1','admin1','admin1@yea.com','admin1','admin1',1);
INSERT INTO "User" VALUES (21,'sad','asd','sadasd@uea.com','sad','asd',0);
INSERT INTO "User" VALUES (26,'Harry','','harryD@uea.com','harry','harry',0);
INSERT INTO "User" VALUES (27,'Customer','Customer','Customer@email.com','Customer','Customer',0);
INSERT INTO "User" VALUES (33,'Cool','Danis','svsb@hotbabe.com','daniscool','123abc',0);
INSERT INTO "User" VALUES (34,'returntest','returntest','return@test.com','return','return',0);
INSERT INTO "User" VALUES (35,'book','book','book@book.com','book','book',0);
INSERT INTO "User" VALUES (36,'custest','custest','custest@hotmail.com','custest','custest',0);
INSERT INTO "User" VALUES (38,'adtest','adtest','adtest@uea.ac.uk','adtest','adtest',1);
INSERT INTO "User" VALUES (39,'test','test','test@uea.ac.uk','test','test',0);
COMMIT;
