/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author toby
 */
public class ManageCustomer {
    //remove user with given username and booking made previously by this user
    public boolean remove(TextField ad_forename, TextField ad_surname, TextField ad_email, TextField ad_usrname, PasswordField ad_password){
        Connection connection = flightbooking.model.Database.getInstance().getConnection();
        Statement statment;
        Boolean user=false;
        
        try {
            statment = connection.createStatement();
            String forename = ad_forename.getText();
            String surname = ad_surname.getText();
            String email = ad_email.getText();
            String username = ad_usrname.getText();
            String password = ad_password.getText();
            ResultSet result = statment.executeQuery("select * from User ");
            while(result.next()){
                if(result.getString("Username").equals(username)){
                    String str = result.getString("UserID");
                    System.out.println(str);
                    statment.executeUpdate("delete from Booking where CustomerID = '"+str+"'");
                    statment.executeUpdate("delete from User where Username = '"+username+"'");
                    user = true;
                }
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ManageCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return user;
    }
    
    //display all customer user accounts and their information for admin to view
    public void displayCustomer(
            TableView table, TableColumn col_email, TableColumn col_firstName, 
            TableColumn col_surname, TableColumn col_id){
        Connection connection = flightbooking.model.Database.getInstance().getConnection();
        ObservableList<flightbooking.model.Customer> oblist = FXCollections.observableArrayList();
        
        try {
            Statement statment = connection.createStatement();
            ResultSet result = statment.executeQuery("select * from User ");
            while (result.next()) {
                if (result.getInt("Admin") == 0) {
                    oblist.add(new flightbooking.model.Customer(result.getInt("UserID"), 
                            result.getString("Forename"), result.getString("Surname"),
                            result.getString("Email")));

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ManageCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        col_email.setCellValueFactory(new PropertyValueFactory<>("email"));
        col_firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        col_surname.setCellValueFactory(new PropertyValueFactory<>("surname"));
        col_id.setCellValueFactory(new PropertyValueFactory<>("ID"));
        table.setItems(oblist);
    }
}
