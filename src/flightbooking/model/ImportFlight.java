/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.model;

import flightbooking.controller.ImportFlightController;
import flightbooking.controller.ManageCustomerController;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;


public class ImportFlight {
    //populate table with all flight information in database for admin to view
    public void displayFlight(TableView flightTable, TableColumn col_flightId, 
            TableColumn col_departure, TableColumn col_arrival, TableColumn col_departureTime,
            TableColumn col_arrivalTime, TableColumn col_price,TableColumn col_bussPrice,TableColumn col_firstPrice){
        Connection connection = Database.getInstance().getConnection();
        ObservableList<flightbooking.model.Flight> flightList = FXCollections.observableArrayList();

        try {
            Statement statment = connection.createStatement();
            ResultSet result1 = statment.executeQuery("select * from FTB");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            //System.out.println(result1.getString("flightNumber"));
            int fn = Integer.parseInt(result1.getString("flightNumber"));
            float price = Float.parseFloat(result1.getString("econPrice"));
            float bussPrice = Float.parseFloat(result1.getString("bussPrice"));
            float firstPrice= Float.parseFloat(result1.getString("firstPrice"));

            //create flight object with data retrieved
            while (result1.next()) {
                Airport dp = Airport.getAirport(result1.getString("departure"));
                Airport ar = Airport.getAirport(result1.getString("arrival"));
                fn = Integer.parseInt(result1.getString("flightNumber"));
                price = Float.parseFloat(result1.getString("econPrice"));
                bussPrice = Float.parseFloat(result1.getString("bussPrice"));
               
                firstPrice = Float.parseFloat(result1.getString("firstPrice"));
                Date date = sdf.parse(result1.getString("departday"));
                Date date2 = sdf.parse(result1.getString("arriveday"));

                Calendar departday = Calendar.getInstance();
                Calendar arrivalday = Calendar.getInstance();
                departday.setTime(date);
                arrivalday.setTime(date2);

                flightList.add(new Flight(fn, dp, ar, departday, arrivalday, price,bussPrice,firstPrice));

            }
        } catch (SQLException ex) {
            Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ImportFlightController.class.getName()).log(Level.SEVERE, null, ex);
        }
        //assign values to different columns
        col_flightId.setCellValueFactory(new PropertyValueFactory<>("flightNum"));
        col_departure.setCellValueFactory(new PropertyValueFactory<>("departure"));
        col_arrival.setCellValueFactory(new PropertyValueFactory<>("arrival"));
        col_departureTime.setCellValueFactory(new PropertyValueFactory<>("departday"));
        col_departureTime.setCellFactory(col -> new TableCell<Flight, Calendar>() {
            @Override
            public void updateItem(Calendar item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null) {
                    setText(null);
                } else {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    setText(sdf.format(item.getTime()));
                }
            }
        });

        col_arrivalTime.setCellValueFactory(new PropertyValueFactory<>("arriveday"));
        col_arrivalTime.setCellFactory(col -> new TableCell<Flight, Calendar>() {
            @Override
            public void updateItem(Calendar item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null) {
                    setText(null);
                } else {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    setText(sdf.format(item.getTime()));
                }
            }
        });
        col_price.setCellValueFactory(new PropertyValueFactory<>("price"));
        col_bussPrice.setCellValueFactory(new PropertyValueFactory<>("bussPrice"));
        col_firstPrice.setCellValueFactory(new PropertyValueFactory<>("firstPrice"));
        flightTable.setItems(flightList);

    }
}
