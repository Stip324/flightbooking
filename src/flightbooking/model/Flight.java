/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author toby
 */
public class Flight {

    private int flightNum;
    private Airport departure;
    private Airport arrival;
    private Calendar departday;
    private Calendar arriveday;
    private long duration;
    private float price;
    private float bussPrice;
    private float firstPrice;
    private Aircraft aircraft;
    private Booking[] bookings;
    static int adult;
    static int children;
    
    public Flight(int flightNum, Airport departure, Airport arrival,
            Calendar departday, Calendar arriveday, float price) {
        this.flightNum = flightNum;
        this.departure = departure;
        this.arrival = arrival;
        this.departday = departday;
        this.arriveday = arriveday;
        this.price = price;
        this.duration = ChronoUnit.HOURS.between(departday.toInstant(), arriveday.toInstant());

    }
    public Flight(int flightNum, Airport departure, Airport arrival,
            Calendar departday, Calendar arriveday, float price,float bussPrice,float firstPrice) {
        this.flightNum = flightNum;
        this.departure = departure;
        this.arrival = arrival;
        this.departday = departday;
        this.arriveday = arriveday;
        this.price = price;
        this.bussPrice = bussPrice;
        this.firstPrice = firstPrice;
        this.duration = ChronoUnit.HOURS.between(departday.toInstant(), arriveday.toInstant());

    }

    public Flight(int flightNum, Airport departure, Airport arrival,
            Calendar departday, Calendar arriveday, float price, Booking[] bookings) {
        this.flightNum = flightNum;
        this.departure = departure;
        this.arrival = arrival;
        this.departday = departday;
        this.arriveday = arriveday;
        this.price = price;
        this.bookings = bookings;
    }

    public int getFlightNum() {
        return flightNum;
    }

    public void setFlightNum(int flightNum) {
        this.flightNum = flightNum;
    }

    public Airport getDeparture() {
        return departure;
    }

    public void setDeparture(Airport departure) {
        this.departure = departure;
    }

    public Airport getArrival() {
        return arrival;
    }

    public void setArrival(Airport arrival) {
        this.arrival = arrival;
    }

    public Calendar getDepartday() {
        return departday;
    }

    //hourOfDay is 24 hour clock. i.e. 4pm = 16
    //month value start with 0. i.e. Jan = 0, Feb = 1...
    public void setDepartday(int year, int month, int date, int hourOfDay, int minute) {
        departday.set(year, month, date, hourOfDay, minute);
    }

    public Calendar getArriveday() {
        return arriveday;
    }

    public void setArriveday(int year, int month, int date, int hourOfDay, int minute) {
        arriveday.set(year, month, date, hourOfDay, minute);
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }
    
    public Aircraft getAircraft(){
        return aircraft;
    }
    
    public void setAircraft(Aircraft aircraft){
        this.aircraft = aircraft;
    }

    public static void setAdultNum(int adult){
        Flight.adult = adult;
    }
    
    public static void setChildrenNum(int children){
        Flight.children = children;
    }
    
    //return calculated price if given children or adult when user search for flight
    //return original price when displaying flight database in admin interface
    public float getPrice() {
        float newPrice;
        if(children != 0){
            double childrenPrice = children * 0.5;
            return newPrice = (float) (price * childrenPrice + price * adult);
        }else if(adult != 0){
            return newPrice = price * adult;
        }else{
            return price;
        }
    }
    public float getBussPrice(){
        return this.bussPrice;
    }
    public float getFirstPrice(){
        return this.firstPrice;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Booking[] getBookings() {
        return bookings;
    }

    public void setBookings(Booking[] bookings) {
        this.bookings = bookings;
    }

    //take parameter and insert flight data in to FTB database
    public static boolean createFlight(int flightNum,Airport departure, 
            Airport arrival, String departday,String arriveday, float econPrice,float bussPrice,float firstPrice) {
        try {
            Connection connection = Database.getInstance().getConnection();
            
            Statement statement = connection.createStatement();

            int status = 0;
            status = statement.executeUpdate("insert into FTB(flightNumber,"
                    + "departure,arrival,departday,arriveday,econPrice,bussPrice,firstPrice)"
                    + "values('" + flightNum + "','" + departure + "','" + arrival 
                    + "','" + departday + "','" + arriveday + "','" + econPrice + "','" + bussPrice + "','" + firstPrice + "')");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Flight.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }
    
    //take flight number and delete corresponding flight
    public static boolean removeFlight(int flightNum){
        try {
            Connection connection = Database.getInstance().getConnection();
            Statement statment = connection.createStatement();
            ResultSet result = statment.executeQuery("select * from FTB ");
            while(result.next()){
                if(Integer.parseInt(result.getString("flightNumber"))== flightNum){
                    statment.executeUpdate("delete from FTB where  flightNumber = '"+flightNum+"'");
                }
            }
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Flight.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
   }
   
   @Override
   public String toString(){
       return flightNum + " " + departure + " " + arrival;
   }
}
