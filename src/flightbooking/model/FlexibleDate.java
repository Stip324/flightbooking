/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author toby
 */
public class FlexibleDate {
    
    //calulate price with number of children and adults selected
    public float calPrice(float price, int adult, int children){
        float newPrice;
        if(children != 0){
            double childrenPrice = children * 0.5;
            return newPrice = (float) (price * childrenPrice + price * adult);
        }else if(adult != 0){
            return newPrice = price * adult;
        }else{
            return price;
        }
    }
    
    //get the lowest seat prices of different classes of the date picked by customer
    public String getClassPrice(LocalDate date, int adult, int children, 
            String selClass){
        Connection connection = Database.getInstance().getConnection();
        String classPrice = "Not Applicable";
        
        try {
            PreparedStatement ps = connection.prepareStatement("select * from "
                    + "FTB where date(departday) = date(?) order by ? desc "
                    + "limit 1");
            ps.setString(1, date.toString());
            ps.setString(2, selClass);
            
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                float price = Float.parseFloat(rs.getString(selClass));
                classPrice = "£" + String.valueOf(calPrice(price, adult, children));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FlexibleDate.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return classPrice;
    }
    
    //get price on specific date/class
    
    public String getEconBeforePrice(LocalDate departDay, int adult, int children,
            String selClass){
        String econBeforePrice = getClassPrice(departDay.minusDays(1), adult, 
                children, selClass);
        return econBeforePrice;
    }
    
    public String getEconOnPrice(LocalDate departDay, int adult, int children, 
            String selClass){
        String econOnPrice = getClassPrice(departDay, adult, children, selClass);
        return econOnPrice;
    }
    
    public String getEconAfterPrice(LocalDate departDay, int adult, int children,
            String selClass){
        String econAfterPrice = getClassPrice(departDay.plusDays(1), adult, children, 
                selClass);
        return econAfterPrice;
    }
    
    public String getBussBeforePrice(LocalDate departDay, int adult, int children,
            String selClass){
        String bussBeforePrice = getClassPrice(departDay.minusDays(1), adult, 
                children, selClass);
        return bussBeforePrice;
    }
    
    public String getBussOnPrice(LocalDate departDay, int adult, int children, 
            String selClass){
        String bussOnPrice = getClassPrice(departDay, adult, children, selClass);
        return bussOnPrice;
    }
    
    public String getBussAfterPrice(LocalDate departDay, int adult, int children,
            String selClass){
        String bussAfterPrice = getClassPrice(departDay.plusDays(1), adult, children, 
                selClass);
        return bussAfterPrice;
    }
    
    public String getFirstBeforePrice(LocalDate departDay, int adult, int children,
            String selClass){
        String firstBeforePrice = getClassPrice(departDay.minusDays(1), adult, 
                children, selClass);
        return firstBeforePrice;
    }
    
    public String getFirstOnPrice(LocalDate departDay, int adult, int children, 
            String selClass){
        String firstOnPrice = getClassPrice(departDay, adult, children, selClass);
        return firstOnPrice;
    }
    
    public String getFirstAfterPrice(LocalDate departDay, int adult, int children,
            String selClass){
        String firstAfterPrice = getClassPrice(departDay.plusDays(1), adult, children, 
                selClass);
        return firstAfterPrice;
    }
}
