/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author toby
 */
public class CustomerBooking {
    private static int customerID;
    private static ObservableList<Flight> booked;
    
    public void setCustomerID(int id){
        this.customerID = id;
    }
    
    //get all booking assoicated with this customer id and get the flight number from booking database
    public static ObservableList<String> getAllBookedFlightNum() throws SQLException{
        Connection connection = Database.getInstance().getConnection();
        ObservableList<String> bookedFlight = FXCollections.observableArrayList();
        
        PreparedStatement ps = connection.prepareStatement("select distinct "
                + "FlightNum from Booking where CustomerID = ? ");
        
        ps.setString(1, String.valueOf(customerID));
            
        //store result for sql statement
        ResultSet result = ps.executeQuery();
        while(result.next()){
            bookedFlight.add(result.getString("FlightNum"));
        }
        return bookedFlight;
    }
    
    //display all booking made by this customer in a table
    public static ObservableList<Flight> displayBooking(TableView booking, TableColumn columnFlightNum, 
            TableColumn columnFrom, TableColumn columnTo, TableColumn columnDepartureDate){
        Connection connection = Database.getInstance().getConnection();
        ObservableList<Flight> availableFlight = FXCollections.observableArrayList();

        try {
            StringBuilder str = new StringBuilder();
            ObservableList<String> bookedFlight = getAllBookedFlightNum();
            for(String s: bookedFlight){
                str.append("'" + s + "'" + ",");
            }
            str.deleteCharAt(str.length() - 1);
            StringBuilder str1 = new StringBuilder();
            str1.append("SELECT * from FTB WHERE flightNumber in (" + str + ")");
            //System.out.println(str1.toString());
            
            Statement s = connection.createStatement();
            ResultSet result = s.executeQuery(str1.toString());
            //System.out.println(result.next());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            while (result.next()) {
                //get each row of information from result set
                Airport dp = Airport.getAirport(result.getString("departure"));
                Airport ar = Airport.getAirport(result.getString("arrival"));
                int fn = Integer.parseInt(result.getString("flightNumber"));
                float price = Float.parseFloat(result.getString("econPrice"));
                Date date = sdf.parse(result.getString("departday"));
                Date date2 = sdf.parse(result.getString("arriveday"));

                Calendar departday = Calendar.getInstance();
                Calendar arrivalday = Calendar.getInstance();
                departday.setTime(date);
                arrivalday.setTime(date2);

                //construct flight object with information in databse and add to list
                availableFlight.add(new Flight(fn, dp, ar, departday, arrivalday, price));
                }
        } catch (SQLException ex) {
            Logger.getLogger(flightbooking.controller.ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(flightbooking.controller.ImportFlightController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //set which table column to display which information of flight
        columnFlightNum.setCellValueFactory(new PropertyValueFactory<>("flightNum"));
        columnFrom.setCellValueFactory(new PropertyValueFactory<>("departure"));
        columnTo.setCellValueFactory(new PropertyValueFactory<>("arrival"));
        columnDepartureDate.setCellValueFactory(new PropertyValueFactory<>("departday"));
        columnDepartureDate.setCellFactory(col -> new TableCell<Flight, Calendar>() {
            @Override
            public void updateItem(Calendar item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null) {
                    setText(null);
                } else {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    setText(sdf.format(item.getTime()));
                }
            }
        });
        
        //populate tableview with the list of flight from result set
        booking.setItems(availableFlight);

        //allow you to select a sigle row in the table
        TableView.TableViewSelectionModel<Flight> sel = booking.getSelectionModel();
        sel.setSelectionMode(SelectionMode.SINGLE);
        //add slected flight to list to be returned
        booked = sel.getSelectedItems();

        //listen for changes in selection
        booked.addListener(new ListChangeListener<Flight>() {
        @Override
        public void onChanged(ListChangeListener.Change<? extends Flight> change) {
            System.out.println("Selection changed");
        }
        });
        return booked;
    }
    
    //refund credit sepent on the ticket to customer
    public static void refundCredit(double refund){
        Connection connection = Database.getInstance().getConnection();
        try {
            PreparedStatement ps = connection.prepareStatement("select * from User where UserID = ?");
            ps.setString(1, Integer.toString(customerID));
            
            ResultSet result = ps.executeQuery();
            while(result.next()){
                double credit = Double.parseDouble(result.getString("Credit"));
                credit += refund;
                PreparedStatement ps1 = connection.prepareStatement("update User set Credit = ? where UserID = ?");
                ps1.setString(1, Double.toString(credit));
                ps1.setString(2, Integer.toString(customerID));
                ps1.execute();
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerBooking.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
