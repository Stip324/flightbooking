/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author toby
 */
public class FlightSelect {
    
    //no sepcified constructor because its not used in the program
    
    /**
     * display required flights and its information in a table for customer to book
     * @param departPort departure airport of the flight
     * @param arrivePort arrival airport of the flight
     * @param d date of which the flight depart
     * @param selClassSeat column name to seat number in selected class
     * @param table table to display on
     * @param flightNum unique id of a flight
     * @param duration  duration in hours between departure time and arrival time
     * @param selClassPrice column name to get price per seat in selected class
     * @param from departure airport
     * @param to arrival airport
     * @param departureTime time when the flight will depart from departPort
     * @return 
     */
    public static ObservableList<Flight> displayFlight(String departPort,
            String arrivePort, LocalDate d, String selClassPrice,String selClassSeat, 
            TableView table, TableColumn flightNum, TableColumn duration, 
            TableColumn seatPrice, TableColumn from, TableColumn to, TableColumn departureTime){
        Connection connection = Database.getInstance().getConnection();
        ObservableList<Flight> availableFlight = FXCollections.observableArrayList();

        try {
            //make sql statement with parameters to enquire database
            PreparedStatement ps = connection.prepareStatement("select * from FTB " + 
                "where departure = ? and arrival = ? and date(departday) = date(?)");
            ps.setString(1, departPort);
            ps.setString(2, arrivePort);
            ps.setString(3, d.toString());

            //store result for sql statement
            ResultSet result = ps.executeQuery();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            while (result.next()) {
                boolean available = Integer.parseInt(result.getString(selClassSeat)) > 0;
                
                if(available){
                    //get each row of information from result set
                    Airport dp = Airport.getAirport(result.getString("departure"));
                    Airport ar = Airport.getAirport(result.getString("arrival"));
                    int fn = Integer.parseInt(result.getString("flightNumber"));
                    float price = Float.parseFloat(result.getString(selClassPrice));
                    Date date = sdf.parse(result.getString("departday"));
                    Date date2 = sdf.parse(result.getString("arriveday"));

                    Calendar departday = Calendar.getInstance();
                    Calendar arrivalday = Calendar.getInstance();
                    departday.setTime(date);
                    arrivalday.setTime(date2);

                    //construct flight object with information in databse and add to list
                    availableFlight.add(new Flight(fn, dp, ar, departday, arrivalday, price));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(flightbooking.controller.ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(flightbooking.controller.ImportFlightController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //set which table column to display which information of flight
        flightNum.setCellValueFactory(new PropertyValueFactory<>("flightNum"));
        from.setCellValueFactory(new PropertyValueFactory<>("departure"));
        to.setCellValueFactory(new PropertyValueFactory<>("arrival"));
        duration.setCellValueFactory(new PropertyValueFactory<>("duration"));
        seatPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
        departureTime.setCellValueFactory(new PropertyValueFactory<>("arriveday"));
        departureTime.setCellFactory(col -> new TableCell<Flight, Calendar>() {
            @Override
            public void updateItem(Calendar item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null) {
                    setText(null);
                } else {
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                    setText(sdf.format(item.getTime()));
                }
            }
        });
        
        //populate tableview with the list of flight from result set
        table.setItems(availableFlight);

        //allow you to select a sigle row in the table
        TableView.TableViewSelectionModel<Flight> sel = table.getSelectionModel();
        sel.setSelectionMode(SelectionMode.SINGLE);
        //add slected flight to list to be returned
        ObservableList<Flight> selected = sel.getSelectedItems();

        //listen for changes in selection
        selected.addListener(new ListChangeListener<Flight>() {
        @Override
        public void onChanged(ListChangeListener.Change<? extends Flight> change) {
            System.out.println("Selection changed");
        }
        });
        
        return selected;
    }
}
