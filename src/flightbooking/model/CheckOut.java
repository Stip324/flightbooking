/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author toby
 */
public class CheckOut {
    private static final ObservableList<Flight> selected = FXCollections.observableArrayList();
    private static String selClassSeat;
    
    public static ObservableList<Flight> getSelected(){
        return selected;
    }
    
    public static void setSelected(ObservableList selected){
        CheckOut.selected.addAll(selected);
    }
    
    public static void setSelClassSeat(String selClassSeat){
        CheckOut.selClassSeat = selClassSeat;
    }
    
    //get the available credit of this customer from the database
    public double findAvailableCredit(int customerID){
        Connection connection = Database.getInstance().getConnection();
        Double credit = 0.0;
        try {
            PreparedStatement ps = connection.prepareStatement("select Credit from "
                    + "User where UserID = ?");
            ps.setString(1, Integer.toString(customerID));
            
            ResultSet result = ps.executeQuery();
            while(result.next()){
                credit = Double.valueOf(result.getString("Credit"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(CheckOut.class.getName()).log(Level.SEVERE, null, ex);
        }
        return credit;
    }
    
    //update customer's available credit
    public void deductCredit(int customerID, double newCredit){
        Connection connection = Database.getInstance().getConnection();
        
        try {
            PreparedStatement ps = connection.prepareStatement("Update User set "
                    + "Credit = ? where UserID = ?");
            ps.setString(1, Double.toString(newCredit));
            ps.setString(2, Integer.toString(customerID));
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CheckOut.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //insert booking info into database
    public void bookFlight(int customerID){
        Connection connection = Database.getInstance().getConnection();
        try {
            Statement statement = connection.createStatement();
            
            for(Flight f: selected){
                int flightNum = f.getFlightNum();
                float flightPrice = f.getPrice();
                statement.executeUpdate("insert into Booking(CustomerID, FlightNum, Price) "
                        + "values ('" + customerID + "', '" + flightNum + "' , "
                                + "'" + flightPrice + "')");
            }
        } catch (SQLException ex) {
            Logger.getLogger(CheckOut.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    //update avilable seat of the chosen flight after booking is made
    public void deductSeats(int adult, int child){
        Connection connection = Database.getInstance().getConnection();
        int seats = 0;

        for(Flight f: selected){
            int flightNum = f.getFlightNum();
            
            try {
            PreparedStatement ps = connection.prepareStatement("select * from FTB where flightNumber = ?");
            ps.setString(1, Integer.toString(flightNum));
            ResultSet result = ps.executeQuery();
                
            while(result.next()){
                seats = Integer.parseInt(result.getString(selClassSeat));
                seats = seats - adult - child;
                }
            } catch (SQLException ex) {
                Logger.getLogger(CheckOut.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                PreparedStatement ps1 = connection.prepareStatement("update FTB set '" + selClassSeat + "' = ? where flightNumber = ?");
                
                ps1.setString(1, Integer.toString(seats));
                ps1.setString(2, Integer.toString(flightNum));
                ps1.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(CheckOut.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
