/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author toby
 */
public class Airport {

    String code;
    String name;
    int flightCapacity;

    public Airport(String code, String name, int flightCapacity) {
        this.code = code;
        this.name = name;
        this.flightCapacity = flightCapacity;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFlightCapacity(int flightCapacity) {
        this.flightCapacity = flightCapacity;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public int getFlightCapacity() {
        return flightCapacity;
    }

    @Override
    public String toString() {
        return this.name;
    }

    //get input and find matching airport in airport database
    public static Airport getAirport(String name) {
        Connection connection = Database.getInstance().getConnection();

        Statement statment;
        try {
            statment = connection.createStatement();
            ResultSet airportResult = statment.executeQuery("select * from Airport");
            while (airportResult.next()) {
                if (airportResult.getString("Name").equalsIgnoreCase(name)) {

                    Airport temp = new Airport(airportResult.getString("code"), 
                            airportResult.getString("name"), Integer.parseInt(
                                    airportResult.getString("flightCapacity")));
                    return temp;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Airport.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
