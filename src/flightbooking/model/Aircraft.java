/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.model;

/**
 *
 * @author toby
 */
public class Aircraft {
    private final int id;
    private final String model;
    private final int totalSeats;
    
    private enum Classes{
        economy(0), bussiness(0), first(0);
    
        private int seats;

        Classes(int n){
            this.seats = n;
        }

        public int getSeats(){
        return seats;
        }
        
        public void setSeats(int seats){
            this.seats = seats;
        }
    };

    public Aircraft(int id, String model, int econSeats, int bussSeats, int firstSeats){
        this.id = id;
        this.model = model;
        this.totalSeats = econSeats + bussSeats + firstSeats;
        
        //set seats for aircraft
        Classes.economy.setSeats(econSeats);
        Classes.bussiness.setSeats(bussSeats);
        Classes.first.setSeats(firstSeats);
    }
    
    public int getId(){
        return id;
    }
    
    public String getModel(){
        return model;
    }
    
    public int getTotalSeats(){
        return totalSeats;
    }
    
    //return true if econ has seats and either bussiness or first has seats
    public boolean classesOpt(){
        return Classes.economy.getSeats() != 0 &&
               (Classes.bussiness.getSeats() != 0|| Classes.first.getSeats() != 0);
    }
    
}
