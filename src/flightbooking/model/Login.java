/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.TextField;

/**
 *
 * @author toby
 */
public class Login {
    
    //query database to if data entered is a vaild user account and return a flag
    public int userLogin(TextField username, TextField password) throws SQLException{
        Connection connection = Database.getInstance().getConnection();
        Statement statement = connection.createStatement();
        String txtusername = username.getText();
        String txtpassword = password.getText();
        int flag = 0;

        ResultSet result = statement.executeQuery("select * from User ");
        while (result.next()) {
            //take to admin interface
            if (result.getString("Username").equals(txtusername) && result.getString("Password").equals(txtpassword)) {
                if(result.getInt("Admin") ==1){
                    flag = 1;
                }
                //take to manage booking
                else if(CheckOut.getSelected().isEmpty()){
                    PreparedStatement ps = connection.prepareStatement("select "
                            + "UserID from User " + "where Username = ? ");
                    ps.setString(1, txtusername);

                    ResultSet result1 = ps.executeQuery();
                    int id = '\n';
                    
                    while(result1.next()){
                        id = Integer.parseInt(result1.getString("UserID"));
                    }
                    Customer c = new Customer();
                    c.setCustomerID(id);
                    flag = 2;
                }
                //check out
                else{
                    flag = 3;
                }
            }
        }
        return flag;
    }
    
    //find customer id with given username input
    public int findCustomerId(TextField username){
        Connection connection = Database.getInstance().getConnection();
        String txtusername = username.getText();
        int customerID = 0;
        try {
            PreparedStatement ps = connection.prepareStatement("select UserID from "
                    + "User where Username = ?");
            ps.setString(1, txtusername);
            
            ResultSet result = ps.executeQuery();
            while(result.next()){
                customerID = Integer.parseInt(result.getString("UserID"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        return customerID;
    }
}
