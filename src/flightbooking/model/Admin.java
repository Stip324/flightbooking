package flightbooking.model;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class Admin extends Account {

    int ID;

    public Admin() {
        this.username = null;
        this.password = null;
        this.firstName = null;
        this.surname = null;
        this.email = null;
    }

    public Admin(String firstname, String surname, String email, String username, String password) {
        if (checkNotEmpty(firstname, surname, email, username, password)) {
            this.username = username;
            this.password = password;
            this.firstName = firstname;
            this.surname = surname;
            if (checkEmail(email)) {
                this.email = email;
            }
        }
    }

    //make user in database
    @Override
    public boolean createAccount(String firstname, String surname, String email, String username, String password) {
        Connection connection = Database.getInstance().getConnection();
        if (checkNotEmpty(firstname, surname, email, username, password)) {
            Admin admin = new Admin(firstname, surname, email, username, password);
            setUsername(username);
            setPassword(password);
            setFirstName(firstname);
            setSurname(surname);
            if (checkEmail(email)) {
                this.email = email;
            }
            int val = 1;

            try {
                Statement statement = connection.createStatement();
                int status = 0;
                status = statement.executeUpdate("insert into User(Forename,Surname,Email,Username,Password,Admin)" + "values('" + firstname + "','" + surname + "','" + email + "','" + username + "','" + password + "','" + val + "')");
            } catch (SQLException ex) {
                Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            }
            return true;
        } else {

            return false;
        }

    }

    //make sure no empty field
    private boolean checkNotEmpty(String a, String b, String c, String d, String e) {
        return !a.isEmpty() || !b.isEmpty() || !c.isEmpty() || !d.isEmpty() || !e.isEmpty();
    }

    private boolean checkEmail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
                + "[a-zA-Z0-9_+&*-]+)*@"
                + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
                + "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null) {
            return false;
        }
        return pat.matcher(email).matches();
    }

    public int getID() {
        return ID;
    }

    public int getAdmin() {
        return admin;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setAdmin(int admin) {
        this.admin = admin;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
