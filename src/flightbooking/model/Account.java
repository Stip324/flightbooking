/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.model;

public class Account {

    int admin = 0;
    String username;
    String password;
    String firstName;
    String surname;
    String email;

    Account() {
        admin = 0;
        username = null;
        password = null;
    }

    public boolean createAccount(String firstname, String surname, String email, String username, String password) {
        if (checkNotEmpty(firstname, surname, email, username, password)) {
            this.username = username;
            this.password = password;
            this.firstName = firstname;
            this.surname = surname;
            this.email = email;
            return true;
        } else {

            return false;
        }

    }
    private void setAdmin(){
        admin =1;
    }
    private boolean checkNotEmpty(String a, String b, String c, String d, String e) {
        return !a.isEmpty() || !b.isEmpty() || !c.isEmpty() || !d.isEmpty() || !e.isEmpty();
    }

}
