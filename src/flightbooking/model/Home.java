/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.model;

import java.sql.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author toby
 */
public class Home {
    //no specified constructor because its not used in the program
    
    //get list of airport names from database
    public static ObservableList<String> getAirportList() throws SQLException{
        Connection connection = Database.getInstance().getConnection();
        Statement statement = connection.createStatement();
        ObservableList<String> allAirports = FXCollections.observableArrayList();
        
        //store result of sql statement in result set
        ResultSet results = statement.executeQuery("select * from Airport ");
        
        while(results.next()){
            //go through result set and add each name to list for return
            allAirports.add(results.getString("Name"));
        }
                
        return allAirports;
    }
    
    //check to see if input airport exist in database
    public static boolean checkAirport(String airport) throws SQLException{
        Connection connection = Database.getInstance().getConnection();
        Statement statement = connection.createStatement();
        //default valid as false
        boolean valid = false;
        
        ResultSet results = statement.executeQuery("select Name from Airport ");
        
        while(results.next()){
            //only if the input matches name in database, valid will be set to true
            if(results.getString("Name").equals(airport)){
                valid = true;
            }
        }
        return valid;
    }
}
