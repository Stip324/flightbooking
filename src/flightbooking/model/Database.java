/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    private Connection connection;
    private static Database instance =null;
    private Database() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            // handle the error
        }
        String connect = "jdbc:sqlite:Account2.db";

        try {
            //connect to database
            connection = DriverManager.getConnection(connect);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Database getInstance() {
        if(instance == null){
            instance =new Database();
        }
        return instance;
    }

    public Connection getConnection() {
        return this.connection;
    }

}
