/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author toby
 */
public class TopUp {
    
    private static final ObservableList<Flight> selected = FXCollections.observableArrayList();
    
    public static ObservableList<Flight> getSelected(){
        return selected;
    }
    
    public static void setSelected(ObservableList selected){
        TopUp.selected.addAll(selected);
    }
    
    //use customer id query database and return credit available
    public double findAvailableCredit(int customerID){
        Connection connection = Database.getInstance().getConnection();
        Double credit = 0.0;
        try {
            PreparedStatement ps = connection.prepareStatement("select Credit from "
                    + "User where UserID = ?");
            ps.setString(1, Integer.toString(customerID));
            
            ResultSet result = ps.executeQuery();
            while(result.next()){
                credit = Double.valueOf(result.getString("Credit"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(CheckOut.class.getName()).log(Level.SEVERE, null, ex);
        }
        return credit;
    }
    
    //update credit associated with the given customer id
    public void topUpCredit(int customerID, double credit,double amount){
        Connection connection = Database.getInstance().getConnection();
        //updated credit = old credit + amount wish to be topped up
        double newCredit = credit + amount;
        try {
            PreparedStatement ps = connection.prepareStatement("update User set "
                    + "Credit = ? where UserID = ?");
            ps.setString(1, Double.toString(newCredit));
            ps.setString(2, Integer.toString(customerID));
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(TopUp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
