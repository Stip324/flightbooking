package flightbooking.model;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class Customer extends Account {

    private int ID;
    private double credit;

    public Customer(int ID,String firstname, String surname, String email) {
        if (checkNotEmpty(firstname, surname, email, username, password)) {
            this.ID = ID;
            this.firstName = firstname;
            this.surname = surname;
            if (checkEmail(email)) {
                this.email = email;

            }
        }
    }

    public Customer(){
        this.username = null;
        this.password = null;
        this.firstName = null;
        this.surname = null;
        this.email = null;
    }
    
    public Customer(String firstname, String surname, String email, String username, 
            String password) {
        this.username = username;
        this.password = password;
        this.firstName = firstname;
        this.surname = surname;
        if (checkEmail(email)) {
            this.email = email;
        }
        this.credit = 0;
    }

    //register customer in user database and book selected flight with their customer id when they sign up
    @Override
    public boolean createAccount(String firstname, String surname, String email,
            String username, String password) {
        Connection connection = Database.getInstance().getConnection();
        
        if (checkNotEmpty(firstname, surname, email, username, password)) {
            Customer customer = new Customer(firstname, surname, email, username, password);
            int val = 0;

            try {
                Statement statement = connection.createStatement();
                int status = 0;
                //inset data into user database
                status = statement.executeUpdate("insert into User(Forename,"
                        + "Surname,Email,Username,Password,Admin, Credit)" + "values('" 
                        + customer.getFirstName() + "','" + customer.getSurname() 
                        + "','" + customer.getEmail() + "','" + customer.getUsername() 
                        + "','" + customer.getPassword() + "','" + val + "','" + 
                        customer.getCredit() + "')");
                //get the auto-generated id associated with customer
                setID(customer);
            } catch (SQLException ex) {
                Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            }
            return true;
        } else {

            return false;
        }
        
    }

    private boolean checkNotEmpty(String a, String b, String c, String d, String e) {
        return !a.isEmpty() || !b.isEmpty() || !c.isEmpty() || !d.isEmpty() || !e.isEmpty();
    }

    private boolean checkEmail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
                + "[a-zA-Z0-9_+&*-]+)*@"
                + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
                + "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null) {
            return false;
        }
        return pat.matcher(email).matches();
    }

    public String getForename(int id){
        Connection connection = Database.getInstance().getConnection();
        String forename = " ";
        try {
            PreparedStatement ps = connection.prepareStatement("select * from User where UserID = ?");
            ps.setString(1, Integer.toString(id));
            
            ResultSet result = ps.executeQuery();
            while(result.next()){
                forename = result.getString("Forename");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return forename;
    }
    
    public int getID() {
        return ID;
    }

    public int getAdmin() {
        return admin;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public double getCredit(){
        return credit;
    }
    
    //get id from given customer
    public void setID(Customer customer) {
        try {
            Connection connection = Database.getInstance().getConnection();
            Statement statment = connection.createStatement();
            ResultSet result = statment.executeQuery("select * from User ");
            while (result.next()) {
                if (result.getString("Username").equals(customer.getUsername()) 
                        && result.getString("Password").equals(customer.getPassword())) {
                    int temp = Integer.parseInt(result.getString("UserID"));
                    this.ID = temp;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setAdmin(int admin) {
        this.admin = admin;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public Customer getCustomer(){
        return this;
    }

    private static int customerID;
    
    public int getCustomerID(){
        return customerID;
    }
    
    public void setCustomerID(int id){
        this.customerID = id;
    }

}
