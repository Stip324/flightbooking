/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author toby
 */
public class Booking {
    int id;
    int flightNum;
    Customer customer;

    public Booking(int id, int flightNum, Customer customer) {
        this.id = id;
        this.flightNum = flightNum;
        this.customer = customer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFlightNum() {
        return flightNum;
    }

    public void setFlightNum(int flightNum) {
        this.flightNum = flightNum;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    
    //take given flight number and remove corresponding flight from booking database
    public static double removeBooking(int flightNum){
        double refund = 0;
        try {
            Connection connection = Database.getInstance().getConnection();
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("select * from Booking ");
            while(result.next()){
                if(Integer.parseInt(result.getString("FlightNum"))== flightNum){
                    refund = Double.parseDouble(result.getString("Price"));
                    statement.executeUpdate("delete from Booking where  FlightNum = '"+flightNum+"'");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Flight.class.getName()).log(Level.SEVERE, null, ex);
        }
        return refund;
    }
}