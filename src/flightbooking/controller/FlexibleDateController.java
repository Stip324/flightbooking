/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.controller;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.stage.Stage;

/**
 *
 * @author toby
 */
public class FlexibleDateController implements Initializable{
    
    private flightbooking.model.FlexibleDate fd = new flightbooking.model.FlexibleDate();
    private String departPort; 
    private String arrivePort;
    private LocalDate departDay;
    private LocalDate returnDay;
    private int adult;
    private int children;
    private String selClass;
    
    @FXML
    private Label dateBefore;
    @FXML
    private Label dateOn;
    @FXML
    private Label dateAfter;
    @FXML
    private Label econBeforePrice;
    @FXML
    private RadioButton econBeforeButton;
    @FXML
    private Label econOnPrice;
    @FXML
    private RadioButton econOnButton;
    @FXML
    private Label econAfterPrice;
    @FXML
    private RadioButton econAfterButton;
    @FXML
    private Label bussBeforePrice;
    @FXML
    private RadioButton bussBeforeButton;
    @FXML
    private Label bussOnPrice;
    @FXML
    private RadioButton bussOnButton;
    @FXML
    private Label bussAfterPrice;
    @FXML
    private RadioButton bussAfterButton;
    @FXML
    private Label firstBeforePrice;
    @FXML
    private RadioButton firstBeforeButton;
    @FXML
    private Label firstOnPrice;
    @FXML
    private RadioButton firstOnButton;
    @FXML
    private Label firstAfterPrice;
    @FXML
    private RadioButton firstAfterButton;
    
    public void setDepartPort(String departPort){
        this.departPort = departPort;
    }
    
    public void setArrivePort(String arrivePort){
        this.arrivePort = arrivePort;
    }
    
    public void setSelClass(String selClass){
        this.selClass = selClass;
    }

    public void setAdult(int adult){
        this.adult = adult;
    }
    
    public void setChildren(int children){
        this.children = children;
    }
    
    public void setDepartDay(LocalDate departDay){
        this.departDay = departDay;
    }
    
    public void setReturnDay(LocalDate returnDay){
        this.returnDay = returnDay;
    }
    
    public void setDateBefore(){
        dateBefore.setText(departDay.minusDays(1).toString());
    }
    
    public void setDateOn(){
        dateOn.setText(departDay.toString());
    }
    
    public void setDateAfter(){
        dateAfter.setText(departDay.plusDays(1).toString());
    }
    
    public void setEconBeforePrice(){
        setSelClass("econPrice");
        econBeforePrice.setText(fd.getEconBeforePrice(departDay, adult, children, 
                selClass));
    }
    
    public void setEconOnPrice(){
        setSelClass("econPrice");
        econOnPrice.setText(fd.getEconOnPrice(departDay, adult, children, selClass));
    }
    
    public void setEconAfterPrice(){
        setSelClass("econPrice");
        econAfterPrice.setText(fd.getEconAfterPrice(departDay, adult, children, 
                selClass));
    }
    
    public void setBussBeforePrice(){
        setSelClass("bussPrice");
        bussBeforePrice.setText(fd.getBussBeforePrice(departDay, adult, children, 
                selClass));
    }
    
    public void setBussOnPrice(){
        setSelClass("bussPrice");
        bussOnPrice.setText(fd.getBussOnPrice(departDay, adult, children, selClass));
    }
    
    public void setBussAfterPrice(){
        setSelClass("bussPrice");
        bussAfterPrice.setText(fd.getEconAfterPrice(departDay, adult, children, 
                selClass));
    }
    
    public void setFirstBeforePrice(){
        setSelClass("firstPrice");
        firstBeforePrice.setText(fd.getEconBeforePrice(departDay, adult, children, 
                selClass));
    }
    
    public void setFirstOnPrice(){
        setSelClass("firstPrice");
        firstOnPrice.setText(fd.getEconOnPrice(departDay, adult, children, selClass));
    }
    
    public void setFirstAfterPrice(){
        setSelClass("firstPrice");
        firstAfterPrice.setText(fd.getEconAfterPrice(departDay, adult, children, 
                selClass));
    }
    
    public void backHome(ActionEvent event){
        Node node = (Node)event.getSource();
        Stage old = (Stage)node.getScene().getWindow();
        
        old.close();
        
        try {
            Parent root1;
            root1 = FXMLLoader.load(getClass().getResource("/flightbooking/view/Home.fxml"));
            
            Stage stage1 = new Stage();
            Scene scene1 = new Scene(root1);

            stage1.setScene(scene1);
            stage1.show();
        } catch (IOException ex) {
            Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //read button selected by customer to display flight that fits their new criteria
    public void seeFlights(ActionEvent event) throws IOException{
        Node node = (Node) event.getSource();
            Stage old = (Stage) node.getScene().getWindow();
            old.close();

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/flightbooking/view/FlightSelect.fxml"));
            Parent root = fxmlLoader.load();
            
            //pass input to FlightSelectController
            FlightSelectController fsc = fxmlLoader.<FlightSelectController>getController();
            fsc.setDepartPort(departPort);
            fsc.setArrivePort(arrivePort);
            fsc.setDepartDay(departDay);
            fsc.setReturnDay(returnDay);
            //fsc.setReturnDay(getReturnDate());
            if(econBeforeButton.isSelected() || econOnButton.isSelected() 
                    || econAfterButton.isSelected()){
                fsc.setSelClass("Economy Class");
            }else if(bussBeforeButton.isSelected() || bussOnButton.isSelected() 
                    ||bussAfterButton.isSelected()){
                fsc.setSelClass("Bussiness Class");
            }else{
                fsc.setSelClass("First Class");
            }

            Stage stage1 = new Stage();
            Scene scene1 = new Scene(root);

            stage1.setScene(scene1);
            stage1.show();
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Platform.runLater(() ->{
            setDateBefore();
            setDateOn();
            setDateAfter();
            
            setEconBeforePrice();
            setEconOnPrice();
            setEconAfterPrice();
            setBussBeforePrice();
            setBussOnPrice();
            setBussAfterPrice();
            setFirstBeforePrice();
            setFirstOnPrice();
            setFirstAfterPrice();
        });
    }
}
