package flightbooking.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

public class homeController implements Initializable {
    
    @FXML
    private Label homeTextStatus;
    
    @FXML
    private TextField departurePort;
    
    @FXML
    private TextField arrivalPort;
    
    @FXML
    private DatePicker departDate;
    //initialise date picker and disable selection on pass dates
    public void initDepartDate(){
        departDate.setDayCellFactory(picker -> new DateCell() {
        public void updateItem(LocalDate date, boolean empty) {
            super.updateItem(date, empty);
            LocalDate today = LocalDate.now();

            setDisable(empty || date.compareTo(today) < 0 );
        }
    });
    }
    
    public LocalDate getDepartDate(){
        LocalDate date = departDate.getValue();
        
        return date;
    }
    
    @FXML
    private DatePicker returnDate;
    //initialise date picker and disable selection on pass dates
    public void initReturnDate(){
        returnDate.setDayCellFactory(picker -> new DateCell() {
        public void updateItem(LocalDate date, boolean empty) {
            super.updateItem(date, empty);
            LocalDate today = LocalDate.now();

            setDisable(empty || date.compareTo(today) < 0 );
        }
    });
    }
    
    public LocalDate getReturnDate(){
        LocalDate date = returnDate.getValue();
        
        return date;
    }
    
    @FXML
    private Spinner adult;    
    //set min(1) and max(10) value of spinner
    public void initAdult(){
        adult.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 10));
    }
    
    public int getAdult(){
        return (Integer) adult.getValue();
    }
    
    @FXML
    private Spinner children;
    //set min(0) and max(10) value of spinner
    public void initChildren(){
        children.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 10));
    }
    
    public int getChildren(){
        return (Integer) children.getValue();
    }
    
    @FXML
    private ListView airportList;
    
    //populate list with avilable airports from database
    public void setAirportList() throws SQLException{
        ObservableList<String> list = flightbooking.model.Home.getAirportList();
        airportList.setItems(list);
    }
    
    @FXML
    private CheckBox flexibleDate;
    
    @FXML
    private ComboBox classes;
    
    public void initClasses(){
        classes.getItems().addAll("Economy Class", "Bussiness Class", "First Class");
    }
    
    public String getClasses(){
        return (String)classes.getValue();
    }
    
    //enable access to login page on home because admin shouldn't need to book 
    //flight before performing admin actions
    @FXML
    public void ToLogin(ActionEvent event){
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();
                
        old.close();
                
        try {
            Parent root1;
            root1 = FXMLLoader.load(getClass().getResource("/flightbooking/view/Login.fxml"));
            
            Stage stage1 = new Stage();
            Scene scene1 = new Scene(root1);

            stage1.setScene(scene1);
            stage1.show();
        } catch (IOException ex) {
            Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean checkEmpty(String a){
        return a.isEmpty();
    }
    
    //check if return date is before departure date
    public boolean checkDate(){
        LocalDate departDay = departDate.getValue();
        LocalDate returnDay = returnDate.getValue();
        
        if(departDay.isEqual(returnDay)){
            return true;
        }
        return departDay.isBefore(returnDay);
    }
    
    //check if departure port and arrival port is the same
    public boolean checkDiffAirport(){
        if(departurePort.getText().equals(arrivalPort.getText())){
            return false;
        }
        return true;
    }
    
    //check for valid input and pass info to either flight select controller or 
    //flexible date controller
    public void flightSelect(ActionEvent event) throws IOException, SQLException{
        String departPort = departurePort.getText();
        String arrivePort = arrivalPort.getText();
        
        //call method written above and check input
        boolean emptyDP = checkEmpty(departPort);
        boolean emptyAP = checkEmpty(arrivePort);
        boolean diffPort = checkDiffAirport();
        boolean validDP = flightbooking.model.Home.checkAirport(departPort);
        boolean validAP = flightbooking.model.Home.checkAirport(arrivePort);
        boolean checkDate;
        boolean isSelected = flexibleDate.isSelected();
        String selClass = getClasses();
        
        //allow return date to be null, so one-way ticket can be booked
        if(getReturnDate() == null){
            checkDate = true;
        }else{
            checkDate = checkDate();
        }

        //error messages
        if(emptyDP){
            homeTextStatus.setText("Please enter your airport of departure");
        }else if(emptyAP){
            homeTextStatus.setText("Please enter your airport of arrival");
        }else if(!checkDate){
            homeTextStatus.setText("Return date cannot be earlier than departure date");
        }else if(!diffPort){
            homeTextStatus.setText("Departure airport and destination cannot be the same");
        }else if(!validDP){
            homeTextStatus.setText("Invalid departure airport");
        }else if(!validAP){
            homeTextStatus.setText("Invalid destination airport");
        }else if(getDepartDate() == null){
            homeTextStatus.setText("Please enter date of travel");
        }else if(selClass == null){
            homeTextStatus.setText("Please select a class");
        }else if(!isSelected){
            //go to FlightSelect page
            Node node = (Node) event.getSource();
            Stage old = (Stage) node.getScene().getWindow();
            old.close();

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/flightbooking/view/FlightSelect.fxml"));
            Parent root = fxmlLoader.load();
            //pass input to FlightSelectController
            FlightSelectController fsc = fxmlLoader.<FlightSelectController>getController();
            fsc.setDepartPort(departPort);
            fsc.setArrivePort(arrivePort);
            fsc.setDepartDay(getDepartDate());
            fsc.setReturnDay(getReturnDate());
            fsc.setSelClass(selClass);
            fsc.setAdult(getAdult());
            fsc.setChild(getChildren());
            
            flightbooking.model.Flight.setAdultNum(getAdult());
            flightbooking.model.Flight.setChildrenNum(getChildren());

            Stage stage1 = new Stage();
            Scene scene1 = new Scene(root);

            stage1.setScene(scene1);
            stage1.show();
        }else{
            Node node = (Node) event.getSource();
            Stage old = (Stage) node.getScene().getWindow();
            old.close();

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/flightbooking/view/FlexibleDate.fxml"));
            Parent root = fxmlLoader.load();
            
            //pass input to FlexibleDateController
            FlexibleDateController fdc = fxmlLoader.<FlexibleDateController>getController();
            fdc.setDepartPort(departPort);
            fdc.setArrivePort(arrivePort);
            fdc.setDepartDay(getDepartDate());
            fdc.setReturnDay(getReturnDate());
            fdc.setAdult(getAdult());
            fdc.setChildren(getChildren());
            
            flightbooking.model.Flight.setAdultNum(getAdult());
            flightbooking.model.Flight.setChildrenNum(getChildren());
            
            Stage stage1 = new Stage();
            Scene scene1 = new Scene(root);

            stage1.setScene(scene1);
            stage1.show();
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        initAdult();
        initChildren();
        initDepartDate();
        initReturnDate();
        initClasses();
        try {
            setAirportList();
        } catch (SQLException ex) {
            Logger.getLogger(homeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
