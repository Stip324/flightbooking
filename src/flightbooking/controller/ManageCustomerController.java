package flightbooking.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Steven
 */
public class ManageCustomerController implements Initializable {

    @FXML
    private TableView<flightbooking.model.Customer> table;
    @FXML
    private TableColumn<flightbooking.model.Customer, String> col_id;
    @FXML
    private TableColumn<flightbooking.model.Customer, String> col_email;
    @FXML
    private TableColumn<flightbooking.model.Customer, String> col_firstName;
    @FXML
    private TableColumn<flightbooking.model.Customer, String> col_surname;
    @FXML
    private TextField ad_usrname;

    @FXML
    private TextField ad_email;

    @FXML
    private PasswordField ad_password;

    @FXML
    private TextField ad_surname;

    @FXML
    private TextField ad_forename;
    @FXML
    private Label label;
    
    private flightbooking.model.ManageCustomer mc = new flightbooking.model.ManageCustomer();

    //popluate table with user database
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        displayCustomer();
    }
    
    public void displayCustomer(){
        mc.displayCustomer(table, col_email, col_firstName, col_surname, col_id);
    }
    
    //read input from text field and pass data to create new customer acc
    //sign up success window pop up if successfully signed up
    @FXML
    public void add(ActionEvent event ) throws ClassNotFoundException, IOException {
            
            String forename = ad_forename.getText();
            String surname = ad_surname.getText();
            String email = ad_email.getText();
            String username = ad_usrname.getText();
            String password = ad_password.getText();
            flightbooking.model.Customer temp = new flightbooking.model.Customer();
            boolean status=false;

            //check no empty fields
            if (checkNotEmpty(forename, surname, email, username, password) && 
                    checkNotNull(ad_usrname, ad_forename, ad_password, ad_surname, 
                            ad_email)&& checkEmail(email)) {
                //update database
                status =temp.createAccount(forename, surname, email, username, password);
                
               
            }
             if (status) {
                System.out.println("user registered");
                
                //get current window
                Node node = (Node) event.getSource();
                Stage old = (Stage) node.getScene().getWindow();
                
                //close current window
                old.close();
                
                //open next window
                Parent root1 = FXMLLoader.load(getClass().getResource("/flightbooking/view/ManageCustomer.fxml"));
                //for every window open need a stage and scene
                Stage stage1 = new Stage();
                Scene scene1 = new Scene(root1);

                stage1.setScene(scene1);
                stage1.show();
                
                Parent root = FXMLLoader.load(getClass().getResource("/flightbooking/view/SignUpSucc.fxml"));
                Stage stage = new Stage();
                Scene scene = new Scene(root);

                stage.setScene(scene);
                stage.show();
            } else {
                if (checkNotNull(ad_usrname, ad_forename, ad_password, ad_surname, ad_email) == false) {
                    label.setText("Sign Up Failed, field(s) required");
                }else if(checkEmail(email) == false){
                    label.setText("Wrong email format");
                }
                else {
                    label.setText("Sign Up Failed");
                }

            }

    }
    
    //get input from username text field and delete data associated with the username
    @FXML
    public void remove(ActionEvent event) throws IOException{
        boolean user = mc.remove(ad_forename, ad_surname, ad_email, ad_usrname, ad_password);
        if(user){
            Node node = (Node) event.getSource();
                Stage old = (Stage) node.getScene().getWindow();
                
                //close current window
                old.close();
                
                //open next window
                Parent root1 = FXMLLoader.load(getClass().getResource("/flightbooking/view/ManageCustomer.fxml"));
                //for every window open need a stage and scene
                Stage stage1 = new Stage();
                Scene scene1 = new Scene(root1);
                stage1.setScene(scene1);
                stage1.show();
        }else{
            label.setText("Removal Failed, No such user");
        }
    }
    
    @FXML
    public void back(ActionEvent event){
        Node node = (Node) event.getSource();
                Stage old = (Stage) node.getScene().getWindow();
                
                //close current window
                old.close();
                
                //open next window
               
        try {
             Parent root1;
            root1 = FXMLLoader.load(getClass().getResource("/flightbooking/view/Admin.fxml"));
            //for every window open need a stage and scene
                Stage stage1 = new Stage();
                Scene scene1 = new Scene(root1);

                stage1.setScene(scene1);
                stage1.show();
        } catch (IOException ex) {
            Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
                
    }
    private boolean checkNotEmpty(String a, String b, String c, String d, String e) {
        return !a.isEmpty() && !b.isEmpty() && !c.isEmpty() && !d.isEmpty() && !e.isEmpty();
    }

    //returm true if a != null
    private boolean checkNotNull(TextField a, TextField b, PasswordField c, TextField d, TextField e) {
        return a.getText() != null && b.getText() != null&&c.getText() != null&&d.getText() != null&&e.getText() != null;
    }
    
    private  boolean checkEmail(String email) 
    { 
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ 
                            "[a-zA-Z0-9_+&*-]+)*@" + 
                            "(?:[a-zA-Z0-9-]+\\.)+[a-z" + 
                            "A-Z]{2,7}$"; 
                              
        Pattern pat = Pattern.compile(emailRegex); 
        if (email == null) 
            return false; 
        return pat.matcher(email).matches(); 
    } 

}
