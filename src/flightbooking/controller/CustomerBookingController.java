package flightbooking.controller;


import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author toby
 */
public class CustomerBookingController implements Initializable{
    ObservableList<flightbooking.model.Flight> booked;
    
    @FXML
    private Label error;
    @FXML
    private TableView<flightbooking.model.Flight> booking;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> columnFlightNum;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> columnFrom;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> columnTo;
    @FXML
    private TableColumn<flightbooking.model.Flight, Calendar> columnDepartureDate;
    
    //display history of all flight booked by this customer
    public void displayBooking(){
        booked = flightbooking.model.CustomerBooking.displayBooking(booking, 
                columnFlightNum, columnFrom, columnTo, columnDepartureDate);
    }

    //allow customer to cancel upcoming booking and refund the credit
    @FXML
    public void cancelBooking(ActionEvent event) throws IOException{
        int flightNum = booked.get(0).getFlightNum();
        Calendar flightdate = booked.get(0).getDepartday();
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        
        //check if booking selected is an upcoming booking
        if(flightdate.before(today)){
            //error message if the flight is leaving today or left before today
            error.setText("Sorry, cannot cancel previous flight.");
        }else{
            //cancel booking and refund credit spent on cancelled booking
            //refresh the page to display the newest booking history
            Double refund = flightbooking.model.Booking.removeBooking(flightNum);
            flightbooking.model.CustomerBooking.refundCredit(refund);
            displayBooking();
        
            Parent root1 = FXMLLoader.load(getClass().getResource("/flightbooking/view/CancelTrip.fxml"));
            Stage stage1 = new Stage();
            Scene scene1 = new Scene(root1);
            stage1.setScene(scene1);
            stage1.show();
        }
    }
    
    @FXML
    public void close(ActionEvent event){
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();

        old.close();
    }

    @FXML
    public void back(ActionEvent event) {
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();

        old.close();
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        error.setText(" ");
        Platform.runLater(() ->{
            displayBooking();
        });
    }
}
