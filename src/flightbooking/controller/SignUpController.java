package flightbooking.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.control.PasswordField;
import javafx.scene.input.MouseEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;


public class SignUpController implements Initializable {

    int customerID;
    private int adult;
    private int child;
    
    @FXML
    private Label signUpStatus;

    @FXML
    private TextField sb_usrname;

    @FXML
    private TextField sb_email;

    @FXML
    private PasswordField sb_password;

    @FXML
    private TextField sb_surname;

    @FXML
    private TextField sb_forename;

    public void setAdult(int adult){
        this.adult = adult;
    }
    
    public void setChild(int child){
        this.child = child;
    }
    
    //create a new customer object in database and take customer to check out page 
    //to complete the purchase
    @FXML
    public void signUp(MouseEvent event) throws ClassNotFoundException, IOException {
        
            String forename = sb_forename.getText();
            String surname = sb_surname.getText();
            String email = sb_email.getText();
            String username = sb_usrname.getText();
            String password = sb_password.getText();
            flightbooking.model.Customer temp = new flightbooking.model.Customer();
            boolean status=false;
            
            if (checkNotEmpty(forename, surname, email, username, password) && checkNotNull(sb_usrname, sb_forename, sb_password, sb_surname, sb_email)&& checkEmail(email)) {
                //update database
                status =temp.createAccount(forename, surname, email, username, password);
                customerID = temp.getID();
            
            }
            if (status) {
                System.out.println("user registered");
                
                //get current window
                Node node = (Node) event.getSource();
                Stage old = (Stage) node.getScene().getWindow();
                
                //close current window
                old.close();
                
                //take customer to check out and take info to check out controller
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/flightbooking/view/CheckOut.fxml"));
                Parent root = fxmlLoader.load();
            
                CheckOutController coc = fxmlLoader.<CheckOutController>getController();
                coc.setCustomerID(customerID);
                coc.setAdult(adult);
                coc.setChild(child);
                
                //for every window open need a stage and scene
                Stage stage = new Stage();
                Scene scene = new Scene(root);

                stage.setScene(scene);
                stage.show();
            } else {
                if (checkNotNull(sb_usrname, sb_forename, sb_password, sb_surname, sb_email) == true) {
                    signUpStatus.setText("Sign Up Failed, field(s) required");
                }else if(checkEmail(email) == false){
                    signUpStatus.setText("Wrong email format");
                }
                else {
                    signUpStatus.setText("Sign Up Failed");
                }

            }

        
    }
    
    //take customer to login
    @FXML
    public void Login(ActionEvent event){
        Node node = (Node) event.getSource();
                Stage old = (Stage) node.getScene().getWindow();
                
                //close current window
                old.close();
                
                //open next window
               
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/flightbooking/view/Login.fxml"));
            Parent root1 = fxmlLoader.load();
            
            //pass input to FlexibleDateController
            LoginController lc = fxmlLoader.<LoginController>getController();
            lc.setAdult(adult);
            lc.setChild(child);
            
            //for every window open need a stage and scene
                Stage stage1 = new Stage();
                Scene scene1 = new Scene(root1);

                stage1.setScene(scene1);
                stage1.show();
        } catch (IOException ex) {
            Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }               
    }
    
    @FXML
    public void backToHome(ActionEvent event){
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();
                
        old.close();
               
        try {
             Parent root1;
            root1 = FXMLLoader.load(getClass().getResource("/flightbooking/view/Home.fxml"));
            //for every window open need a stage and scene
                Stage stage1 = new Stage();
                Scene scene1 = new Scene(root1);

                stage1.setScene(scene1);
                stage1.show();
        } catch (IOException ex) {
            Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
                
    }

    //return true if length of text is 0
    private boolean checkNotEmpty(String a, String b, String c, String d, String e) {
        return !a.isEmpty() && !b.isEmpty() && !c.isEmpty() && !d.isEmpty() && !e.isEmpty();
    }

    //returm true if a != null
    private boolean checkNotNull(TextField a, TextField b, PasswordField c, TextField d, TextField e) {
        return a.getText() != null && b.getText() != null&&c.getText() != null&&d.getText() != null&&e.getText() != null;
    }

    private  boolean checkEmail(String email) 
    { 
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ 
                            "[a-zA-Z0-9_+&*-]+)*@" + 
                            "(?:[a-zA-Z0-9-]+\\.)+[a-z" + 
                            "A-Z]{2,7}$"; 
                              
        Pattern pat = Pattern.compile(emailRegex); 
        if (email == null) 
            return false; 
        return pat.matcher(email).matches(); 
    } 
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
}
