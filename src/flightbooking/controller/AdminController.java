/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AdminController implements Initializable {


    //take user to page to sign up another admin
    public void register(ActionEvent event) throws Exception {
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();
        old.close();
        Parent root = FXMLLoader.load(getClass().getResource("/flightbooking/view/AdminSignUp.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
    }

    //take user to page to manage flight in database
    public void importflight(ActionEvent event) throws Exception {
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();
        old.close();
        Parent root = FXMLLoader.load(getClass().getResource("/flightbooking/view/importFlight.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
    }

    //take user to page to manage customer account in database
    public void manage(ActionEvent event) throws Exception {
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();
        old.close();
        Parent root = FXMLLoader.load(getClass().getResource("/flightbooking/view/ManageCustomer.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
    }

    //take user back to log in page
    @FXML
    public void Logout(ActionEvent event) {
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();

        old.close();

        try {
            Parent root1;
            root1 = FXMLLoader.load(getClass().getResource("/flightbooking/view/Login.fxml"));
            Stage stage1 = new Stage();
            Scene scene1 = new Scene(root1);

            stage1.setScene(scene1);
            stage1.show();
        } catch (IOException ex) {
            Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

}
