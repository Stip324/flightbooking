/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.controller;

import flightbooking.model.Flight;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 *
 * @author toby
 */
public class CheckOutController implements Initializable {
    
    private flightbooking.model.CheckOut checkoutModel;
    private static ObservableList<Flight> selected = flightbooking.model.CheckOut.getSelected();
    private double total = 0;
    private double credit;
    private int customerID;
    private int adult;
    private int child;
    
    @FXML
    private TableView table;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> columnFlightNum;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> columnDuration;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> columnPrice;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> columnFrom;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> columnTo;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> columnDepartureTime;
    @FXML
    private Label totalPrice;
    @FXML
    private Label availableCredit;

    public CheckOutController() {
        this.checkoutModel = new flightbooking.model.CheckOut();
    }
    
    public void setCustomerID(int customerID){
        this.customerID = customerID;
    }
    
    public void setAdult(int adult){
        this.adult = adult;
    }
    
    public void setChild(int child){
        this.child = child;
    }
    
    //display to customer their selected flight before they check out
    public void displaySelectedFlights(TableColumn columnDepartureTime){
        columnFlightNum.setCellValueFactory(new PropertyValueFactory<>("flightNum"));
        columnFrom.setCellValueFactory(new PropertyValueFactory<>("departure"));
        columnTo.setCellValueFactory(new PropertyValueFactory<>("arrival"));
        columnDuration.setCellValueFactory(new PropertyValueFactory<>("duration"));
        columnPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
        columnDepartureTime.setCellValueFactory(new PropertyValueFactory<>("arriveday"));
        columnDepartureTime.setCellFactory(col -> new TableCell<Flight, Calendar>() {
            @Override
            public void updateItem(Calendar item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null) {
                    setText(null);
                } else {
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                    setText(sdf.format(item.getTime()));
                }
            }
        });
        
        //populate tableview with the list of flight from result set
        table.setItems(selected);
    }

    //calculate the total of their purchase
    public void setTotalPrice(){
        for(flightbooking.model.Flight f: selected){
            total += f.getPrice();
        }
        totalPrice.setText(Double.toString(total));
    }
    
    //display to customer their available credit
    public void setAvailableCredit(){
        credit = checkoutModel.findAvailableCredit(customerID);
        availableCredit.setText(String.valueOf(credit));
    }
    
    //book flight for customer, check if they have enough credit to make the purchase.
    //if yes, deduct their available credit, available seat from their chosen flight
    @FXML
    public void bookFlight(ActionEvent event){
        //enough credit
        if(credit >= total){
            double newCredit = credit - total;
            checkoutModel.bookFlight(customerID);
            checkoutModel.deductCredit(customerID, newCredit);
            checkoutModel.deductSeats(adult, child);
            flightbooking.model.CheckOut.getSelected().clear();
            
            Node node = (Node) event.getSource();
            Stage old = (Stage) node.getScene().getWindow();
                
            old.close();
                
            try {
                Parent root = FXMLLoader.load(getClass().getResource("/flightbooking/view/Home.fxml"));
            
                Stage stage = new Stage();
                Scene scene = new Scene(root);

                stage.setScene(scene);
                stage.show();
                
                Parent root1 = FXMLLoader.load(getClass().getResource("/flightbooking/view/BookFlightSucc.fxml"));
                
                Stage stage1 = new Stage();
                Scene scene1 = new Scene(root1);

                stage1.setScene(scene1);
                stage1.show();
            } catch (IOException ex) {
                Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
            }    
        }else{
            try {
                //error message pops up if customer doesn't have enough credit
                Parent root = FXMLLoader.load(getClass().getResource("/flightbooking/view/InsufficientCred.fxml"));
                
                Stage stage = new Stage();
                Scene scene = new Scene(root);
                
                stage.setScene(scene);
                stage.show();
            } catch (IOException ex) {
                Logger.getLogger(CheckOutController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }
    
    //get rid of pop ups
    @FXML
    public void close(ActionEvent event) {
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();
        old.close();
    }
    
    //top up page pop up
    @FXML
    public void topUp(ActionEvent event){
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();
        old.close();
        
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/flightbooking/view/TopUp.fxml"));
            Parent root = fxmlLoader.load();
            
            TopUpController tuc = fxmlLoader.<TopUpController>getController();
            tuc.setCustomerID(customerID);
            tuc.setAdult(adult);
            tuc.setChild(child);
            flightbooking.model.TopUp.setSelected(selected);
            
            Stage stage = new Stage();
            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(CheckOutController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //cancel check out and back to home page
    @FXML
    public void back(ActionEvent event){
        flightbooking.model.CheckOut.getSelected().clear();
        
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();
        old.close();
        
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/flightbooking/view/Home.fxml"));
            Parent root = fxmlLoader.load();
            
            Stage stage = new Stage();
            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(CheckOutController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        displaySelectedFlights(columnDepartureTime);
        setTotalPrice();
        Platform.runLater(()->{
            setAvailableCredit();
        });
    }
}
