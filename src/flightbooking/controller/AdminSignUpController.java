package flightbooking.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.control.PasswordField;
import javafx.scene.input.MouseEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class AdminSignUpController implements Initializable {

    @FXML
    private Label signUpStatus;

    @FXML
    private TextField sb_usrname;

    @FXML
    private TextField sb_email;

    @FXML
    private PasswordField sb_password;

    @FXML
    private TextField sb_surname;

    @FXML
    private TextField sb_forename;

    //get input from text field, do data check and register account in database
    @FXML
    public void signUp(MouseEvent event) throws ClassNotFoundException, IOException {      
            Class.forName("org.sqlite.JDBC");
            
            
            String forename = sb_forename.getText();
            String surname = sb_surname.getText();
            String email = sb_email.getText();
            String username = sb_usrname.getText();
            String password = sb_password.getText();
            flightbooking.model.Admin temp = new flightbooking.model.Admin();
            boolean status=false;
            
            Boolean fieldAlert = false;
            
            if (checkNotEmpty(forename, surname, email, username, password) && 
                    checkNotNull(sb_usrname, sb_forename, sb_password, sb_surname, 
                            sb_email)&& checkEmail(email)) {
                //update database
                status =temp.createAccount(forename, surname, email, username, password);
                
               
            }

            if (status) {
                System.out.println("user registered");
                
                //get current window
                Node node = (Node) event.getSource();
                Stage old = (Stage) node.getScene().getWindow();
                
                //close current window
                old.close();
                
                //open next window
                Parent root1 = FXMLLoader.load(getClass().getResource("/flightbooking/view/Login.fxml"));
                //for every window open need a stage and scene
                Stage stage1 = new Stage();
                Scene scene1 = new Scene(root1);

                stage1.setScene(scene1);
                stage1.show();
                
                Parent root = FXMLLoader.load(getClass().getResource("/flightbooking/view/SignUpSucc.fxml"));
                Stage stage = new Stage();
                Scene scene = new Scene(root);

                stage.setScene(scene);
                stage.show();
            } else {
                if (checkNotNull(sb_usrname, sb_forename, sb_password, sb_surname, sb_email) == false) {
                    signUpStatus.setText("Sign Up Failed, field(s) required");
                }else if(checkEmail(email) == false){
                    signUpStatus.setText("Wrong email format");
                }
                else {
                    signUpStatus.setText("Sign Up Failed");
                }

            }

        
    }
    
    //take user back to admin interface
    @FXML
    public void backToAdmin(ActionEvent event){
        Node node = (Node) event.getSource();
                Stage old = (Stage) node.getScene().getWindow();
                
                //close current window
                old.close();
                
                //open next window
               
        try {
             Parent root1;
            root1 = FXMLLoader.load(getClass().getResource("/flightbooking/view/Admin.fxml"));
            //for every window open need a stage and scene
                Stage stage1 = new Stage();
                Scene scene1 = new Scene(root1);

                stage1.setScene(scene1);
                stage1.show();
        } catch (IOException ex) {
            Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
                
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    //return true if length of text is 0
    private boolean checkNotEmpty(String a, String b, String c, String d, String e) {
        return !a.isEmpty() && !b.isEmpty() && !c.isEmpty() && !d.isEmpty() && !e.isEmpty();
    }

    //returm true if any field != null
    private boolean checkNotNull(TextField a, TextField b, TextField c, TextField d, TextField e) {
        return a.getText() != null && b.getText() != null&&c.getText() != null&&d.getText() != null&&e.getText() != null;
    }

    @FXML
    public void close(ActionEvent event) {
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();
        old.close();
    }
    private  boolean checkEmail(String email) 
    { 
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ 
                            "[a-zA-Z0-9_+&*-]+)*@" + 
                            "(?:[a-zA-Z0-9-]+\\.)+[a-z" + 
                            "A-Z]{2,7}$"; 
                              
        Pattern pat = Pattern.compile(emailRegex); 
        if (email == null) 
            return false; 
        return pat.matcher(email).matches(); 
    } 
}
