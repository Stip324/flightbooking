/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.controller;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author toby
 */
public class FlightSelectController implements Initializable {

    private String departPort;
    private String arrivePort;
    private LocalDate departDay;
    private LocalDate returnDay;
    private String selClassPrice;
    private String selClassSeat;
    private ObservableList<flightbooking.model.Flight> selected;
    private int counter = 0;
    private int adult;
    private int child;
    
    @FXML
    private TableView table;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> columnFlightNum;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> columnDuration;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> columnPrice;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> columnFrom;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> columnTo;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> columnDepartureTime;
    
    public void setDepartPort(String departPort){
        this.departPort = departPort;
    }
    
    public void setArrivePort(String arrivePort){
        this.arrivePort = arrivePort;
    }
    
    public void setDepartDay(LocalDate departDay){
        this.departDay = departDay;
    }
    
    public void setReturnDay(LocalDate returnDay){
        this.returnDay = returnDay;
    }
    
    //read customer input and set values to query database later
    public void setSelClass(String selClass){
        switch (selClass) {
            case "First Class":
                this.selClassPrice = "firstPrice";
                this.selClassSeat = "firstSeat";
                break;
            case "Bussiness Class":
                this.selClassPrice = "bussPrice";
                this.selClassSeat = "bussSeat";
                break;
            default:
                this.selClassPrice = "econPrice";
                this.selClassSeat = "econSeat";
                break;
        }
    }
    
    public void setAdult(int adult){
        this.adult = adult;
    }
    
    public void setChild(int child){
        this.child = child;
    }
    
    public ObservableList getSelected(){
        return selected;
    }
    
    public TableView getTable(){
        return table;
    }
    
    //call displayFlight from FlightSelect model class and assign this table and
    //its columns to it
    //also update selected for booking pupose later
    public void displayFlight(String departPort, String arrivePort, LocalDate d,
            String selClassPrice, String selClassSeat){
        selected = flightbooking.model.FlightSelect.displayFlight(departPort, 
                arrivePort, d, selClassPrice, selClassSeat, table, columnFlightNum, columnDuration, 
                columnPrice, columnFrom, columnTo, columnDepartureTime);
    }
    
    //home page button
    @FXML
    public void backToHome(ActionEvent event){
        if(!getSelected().isEmpty()){
            getSelected().clear();
        }
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();
                
        old.close();               
        try {
            Parent root1;
            root1 = FXMLLoader.load(getClass().getResource("/flightbooking/view/Home.fxml"));

            Stage stage1 = new Stage();
            Scene scene1 = new Scene(root1);

            stage1.setScene(scene1);
            stage1.show();
        } catch (IOException ex) {
            Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    //go to signup page and proceed booking or update table to display available 
    //return flight
    @FXML
    public void bookFlight(ActionEvent event){
        //pass selected flight to both classes so the customer's id is associated 
        //with the flight when the booking is made
        flightbooking.model.CheckOut.setSelected(selected);
        flightbooking.model.CheckOut.setSelClassSeat(selClassSeat);

        //if no return flight needed, proceed to signup
        if(returnDay == null || counter > 0){
            Node node = (Node) event.getSource();
            Stage old = (Stage) node.getScene().getWindow();

            old.close();                          
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/flightbooking/view/SignUp.fxml"));
                Parent root1 = fxmlLoader.load();
            
                //pass input to FlexibleDateController
                SignUpController suc = fxmlLoader.<SignUpController>getController();
                suc.setAdult(adult);
                suc.setChild(child);

                Stage stage1 = new Stage();
                Scene scene1 = new Scene(root1);

                stage1.setScene(scene1);
                stage1.show();
            } catch (IOException ex) {
                Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            //update table to display return flight info
            displayFlight(arrivePort, departPort, returnDay, selClassPrice, selClassSeat);
            counter++;
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Platform.runLater(() ->{
            displayFlight(departPort, arrivePort, departDay, selClassPrice, selClassSeat);
        });
    }
    
}
