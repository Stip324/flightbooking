/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 *
 * @author toby
 */
public class CustomerController implements Initializable{
    
    private flightbooking.model.Customer c;
    
    public CustomerController(){
        c = new flightbooking.model.Customer();
    }
    
    @FXML
    private Label forename;
    
    public void setForename(){
        forename.setText(c.getForename(c.getCustomerID()));
    }
    
    @FXML
    public void Logout(ActionEvent event) {
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();

        old.close();

        try {
            Parent root1;
            root1 = FXMLLoader.load(getClass().getResource("/flightbooking/view/Login.fxml"));
            Stage stage1 = new Stage();
            Scene scene1 = new Scene(root1);

            stage1.setScene(scene1);
            stage1.show();
        } catch (IOException ex) {
            Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //take customer to manage their booked flights
    @FXML
    public void ManageBooking(ActionEvent event){
        flightbooking.model.CustomerBooking cb = new flightbooking.model.CustomerBooking();
        cb.setCustomerID(c.getCustomerID());
        
        try {
            Parent root1 = FXMLLoader.load(getClass().getResource("/flightbooking/view/CustomerBooking.fxml"));
            Stage stage1 = new Stage();
            Scene scene1 = new Scene(root1);

            stage1.setScene(scene1);
            stage1.show();
        } catch (IOException ex) {
            Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //take customer to top up their account
    @FXML
    public void TopUp(ActionEvent event){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/flightbooking/view/TopUp.fxml"));
            Parent root1 = fxmlLoader.load();
            
            TopUpController tuc = fxmlLoader.<TopUpController>getController();
            tuc.setCustomerID(c.getCustomerID());
            
            Stage stage1 = new Stage();
            Scene scene1 = new Scene(root1);

            stage1.setScene(scene1);
            stage1.show();
        } catch (IOException ex) {
            Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setForename();
    }
}
