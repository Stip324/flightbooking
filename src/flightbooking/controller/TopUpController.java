/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.controller;

import flightbooking.model.Flight;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author toby
 */
public class TopUpController implements Initializable{
    
    private flightbooking.model.TopUp topUpModel;
    private int customerID;
    private double availableCredit;
    private double amount;
    private static ObservableList<Flight> selected = flightbooking.model.TopUp.getSelected();
    private int adult;
    private int child;
    
    @FXML
    private Label txtavailableCredit;
    @FXML
    private TextField txtamount;
    
    public TopUpController(){
        this.topUpModel = new flightbooking.model.TopUp();
    }
    
    public void setCustomerID(int customerID){
        this.customerID = customerID;
    }
    
    //query database in model class to get the amount of credit this particular 
    //customer has
    public void setAvailableCredit(){
        availableCredit = topUpModel.findAvailableCredit(customerID);
    }
    
    //display the credit to customer
    public void setTxtAvailableCredit(){
        txtavailableCredit.setText(String.valueOf(availableCredit));
    }
    
    public void setAmount(){
        amount = Double.parseDouble(txtamount.getText());
    }
    
    public void setAdult(int adult){
        this.adult = adult;
    }
    
    public void setChild(int child){
        this.child = child;
    }
    
    //add top up amount to this customer account and refresh the page to display 
    //the newest credit of the account
    @FXML
    public void TopUp(ActionEvent event){
        setAmount();
        topUpModel.topUpCredit(customerID, availableCredit, amount);
        
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();
        
        old.close();
        
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/flightbooking/view/TopUp.fxml"));
            Parent root = fxmlLoader.load();
            
            TopUpController tuc = fxmlLoader.<TopUpController>getController();
            tuc.setCustomerID(customerID);
                
            Stage stage = new Stage();
            Scene scene = new Scene(root);
                
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(CheckOutController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //take customer back to where they came from
    @FXML
    public void back(ActionEvent event){
        //if customer has selected flight meaning they came from check out page
        if(!selected.isEmpty()){
            Node node = (Node) event.getSource();
            Stage old = (Stage) node.getScene().getWindow();

            old.close();

            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/flightbooking/view/CheckOut.fxml"));
                Parent root = fxmlLoader.load();

                CheckOutController coc = fxmlLoader.<CheckOutController>getController();
                coc.setCustomerID(customerID);
                coc.setAdult(adult);
                coc.setChild(child);
                flightbooking.model.CheckOut.getSelected().clear();
                flightbooking.model.CheckOut.setSelected(selected);
                flightbooking.model.TopUp.getSelected().clear();


                Stage stage = new Stage();
                Scene scene = new Scene(root);

                stage.setScene(scene);
                stage.show();
            } catch (IOException ex) {
                Logger.getLogger(CheckOutController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            Node node = (Node) event.getSource();
            Stage old = (Stage) node.getScene().getWindow();
            
            old.close();
        }
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Platform.runLater(()->{
            setAvailableCredit();
            setTxtAvailableCredit();
        });
    }
}
