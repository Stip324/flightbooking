/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flightbooking.controller;

import flightbooking.model.Airport;
import flightbooking.model.Flight;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * FXML Controller class
 *
 * @author Steven
 */
public class ImportFlightController implements Initializable {

    @FXML
    private Label error;
    
    @FXML
    private TableView<flightbooking.model.Flight> flightTable;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> col_flightId;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> col_departure;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> col_arrival;
    @FXML
    private TableColumn<flightbooking.model.Flight, Calendar> col_departureTime;
    @FXML
    private TableColumn<flightbooking.model.Flight, Calendar> col_arrivalTime;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> col_price;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> col_bussPrice;
    @FXML
    private TableColumn<flightbooking.model.Flight, String> col_firstPrice;
    @FXML
    private TextField tf_flightID;
    @FXML
    private TextField tf_depature;
    @FXML
    private TextField tf_arrival;
    @FXML
    private TextField tf_depatureTime;
    @FXML
    private TextField tf_arrivalTime;
    @FXML
    private TextField tf_price;
    @FXML
    private TextField tf_bussPrice;
    @FXML
    private TextField tf_firstPrice;

    //poppulate table view with FTB database
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        flightbooking.model.ImportFlight imFlight = new flightbooking.model.ImportFlight();
        imFlight.displayFlight(flightTable, col_flightId, col_departure, col_arrival, col_departureTime, col_arrivalTime, col_price,col_bussPrice,col_firstPrice);
    }

    //take user back to admin interface
    @FXML
    public void backToAdmin(ActionEvent event) {
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();

        //close current window
        old.close();

        //open next window
        try {
            Parent root1;
            root1 = FXMLLoader.load(getClass().getResource("/flightbooking/view/Admin.fxml"));
            //for every window open need a stage and scene
            Stage stage1 = new Stage();
            Scene scene1 = new Scene(root1);

            stage1.setScene(scene1);
            stage1.show();
        } catch (IOException ex) {
            Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //take input from text field and create new flight in FTB database
    @FXML
    public void addFlight(ActionEvent event) throws ClassNotFoundException, IOException{
        //read input
        String temp = tf_flightID.getText();
        int flighId = Integer.parseInt(temp);
        String dp = tf_depature.getText();
        String ar = tf_arrival.getText();
        String dateDp = tf_depatureTime.getText();
        String dateAr = tf_arrivalTime.getText();
        float price1 = Float.parseFloat(tf_price.getText());
        float price2 = Float.parseFloat(tf_bussPrice.getText());
        float price3 = Float.parseFloat(tf_firstPrice.getText());

        if (checkNotNull(tf_flightID, tf_depature, tf_arrival, tf_depatureTime, 
                tf_arrivalTime, tf_price)) {
            //createFlight method in flight add data to database
            flightbooking.model.Flight.createFlight(flighId, flightbooking.model.Airport.getAirport(dp), 
                    flightbooking.model.Airport.getAirport(ar), dateDp, dateAr, price1,price2,price3);
            
            Node node = (Node) event.getSource();
            Stage old = (Stage) node.getScene().getWindow();

            //close current window
            old.close();

            //open next window
            Parent root1 = FXMLLoader.load(getClass().getResource("/flightbooking/view/importFlight.fxml"));
            //for every window open need a stage and scene
            Stage stage1 = new Stage();
            Scene scene1 = new Scene(root1);
            stage1.setScene(scene1);
            stage1.show();

        } else {
            System.err.println("Field(s) empty");
        }

    }

    //read input from text field and remove enquired flight
    @FXML
    public void removeFlight(ActionEvent event) throws IOException {
        //read input
        String temp = tf_flightID.getText();
        int flighId = Integer.parseInt(temp);
        
        //pass input to removeFlight to delete record from FTB database
        flightbooking.model.Flight.removeFlight(flighId);
        
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();

        //close current window
        old.close();

        //open next window
        Parent root1 = FXMLLoader.load(getClass().getResource("/flightbooking/view/importFlight.fxml"));
        //for every window open need a stage and scene
        Stage stage1 = new Stage();
        Scene scene1 = new Scene(root1);
        stage1.setScene(scene1);
        stage1.show();
    }
    @FXML
    void importFile(ActionEvent event) throws FileNotFoundException, IOException {
  
        int flightId;
        String dp;
        String ar;
        String dateDp;
        String dateAr;
        float ecoPrice;
        float bussPrice;
        float firsrPrice;
        float econSeats;
        float busSeats;
        float firstSeats;
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new ExtensionFilter("Excel Files", "*.xlsx"));
        File file = fc.showOpenDialog(null);
        FileInputStream flights = new FileInputStream(file);
        //FileInputStream fileIn = new FileInputStream(new File("flight.xlsx"));
        XSSFWorkbook wb = new XSSFWorkbook(flights);
        XSSFSheet sheet = wb.getSheetAt(0);
        Row row;
        for (int i = 1; i <= sheet.getLastRowNum(); i++) {
            row = sheet.getRow(i);
            flightId = (int) row.getCell(0).getNumericCellValue();
            dp = row.getCell(1).getStringCellValue();
            ar = row.getCell(2).getStringCellValue();
            dateDp = row.getCell(3).getStringCellValue();
            dateAr = row.getCell(4).getStringCellValue();
            ecoPrice = (float) row.getCell(5).getNumericCellValue();
            bussPrice = (float) row.getCell(6).getNumericCellValue();
            firsrPrice = (float) row.getCell(7).getNumericCellValue();
            econSeats = (float) row.getCell(8).getNumericCellValue();
            busSeats = (float) row.getCell(9).getNumericCellValue();
            firstSeats = (float) row.getCell(10).getNumericCellValue();
            //System.out.println(flightId + dp + ar + dateDp + dateAr + ecoPrice);
            Flight.createFlight(flightId, Airport.getAirport(dp),
                        Airport.getAirport(ar), dateDp, dateAr, ecoPrice,bussPrice,firsrPrice);
          
            Node node = (Node) event.getSource();
            Stage old = (Stage) node.getScene().getWindow();

            //close current window
            old.close();

            //open next window
            Parent root1 = FXMLLoader.load(getClass().getResource("/flightbooking/view/importFlight.fxml"));
            //for every window open need a stage and scene
            Stage stage1 = new Stage();
            Scene scene1 = new Scene(root1);
            stage1.setScene(scene1);
            stage1.show();


 

        }
    }


    //returm true if a != null
    private boolean checkNotNull(TextField a, TextField b, TextField c, TextField d, TextField e, TextField f) {
        return a.getText() != null && b.getText() != null && c.getText() != null && d.getText() != null && e.getText() != null && f.getText() != null;
    }
}
