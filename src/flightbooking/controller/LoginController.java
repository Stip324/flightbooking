package flightbooking.controller;

import java.io.IOException;
import java.net.URL;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.fxml.FXML;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.Node;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    int customerID;
    private int adult;
    private int child;
    
    @FXML
    private Label status;
    @FXML
    private TextField usrname;
    @FXML
    private TextField password;
    
    public void setAdult(int adult){
        this.adult = adult;
    }
    
    public void setChild(int child){
        this.child = child;
    }
    
    //get input compare to records in user database
    //if acc is flagged admin(1), take to admin interface
    //if acc is flagged customer(0), and has previously selected flight(s), take to check out
    //if acc is flagged customer(0), but no flight was selected, take to manage bookings
    //if input doesn't match any record in user database, error massage 'login fail'
    public void login(ActionEvent event) throws Exception {
        flightbooking.model.Login loginModel = new flightbooking.model.Login();
        int flag = loginModel.userLogin(usrname, password);
        customerID = loginModel.findCustomerId(usrname);
        
        if(flag == 1){
            Node node = (Node) event.getSource();
            Stage old = (Stage) node.getScene().getWindow();
            old.close();
            Parent root = FXMLLoader.load(getClass().getResource("/flightbooking/view/Admin.fxml"));
            Stage stage = new Stage();
            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.show();
        }
        //take to manage booking
        else if(flag == 2){
            Node node = (Node) event.getSource();
            Stage old = (Stage) node.getScene().getWindow();
            old.close();
                    
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/flightbooking/view/Customer.fxml"));
            Parent root = fxmlLoader.load();
                    
            Stage stage = new Stage();
            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.show();
        }
        //check out
        else if(flag == 3){
            status.setText("Login Success");
            Node node = (Node) event.getSource();
            Stage old = (Stage) node.getScene().getWindow();
            old.close();
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/flightbooking/view/CheckOut.fxml"));
            Parent root = fxmlLoader.load();
            
            CheckOutController coc = fxmlLoader.<CheckOutController>getController();
            coc.setCustomerID(customerID);
            coc.setAdult(adult);
            coc.setChild(child);
            
            Stage stage = new Stage();
            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.show();
        }else{
            status.setText("Login Failed");
        }
    }

    //take user to sign up page
    public void signUp(ActionEvent event) throws Exception {
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();
        old.close();
        Parent root = FXMLLoader.load(getClass().getResource("/flightbooking/view/SignUp.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
    }
    
    //take to search flight
    @FXML
    public void backToHome(ActionEvent event) throws IOException{
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();
        old.close();
        Parent root = FXMLLoader.load(getClass().getResource("/flightbooking/view/Home.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
    }
    
    //get rid of pop ups
    @FXML
    public void close(ActionEvent event) {
        Node node = (Node) event.getSource();
        Stage old = (Stage) node.getScene().getWindow();
        old.close();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
