/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import flightbooking.*;
import java.sql.SQLException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author toby
 */
public class NewEmptyJUnitTest {
    Customer customer;
    
    public NewEmptyJUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        customer = new Customer();
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCreateAccount(){
        assertEquals(true, customer.createAccount("Jtest", "Jtest", 
                "Jtest@uea.ac.uk", "Jtest", "Jtest"));
    }
    
    @Test
    public void testCheckAirport() throws SQLException{
        assertEquals(false, Home.checkAirport("Taiwan"));
    }
}
